var gulp = require('gulp');
var browserSync = require('browser-sync');

// this task utilizes the browsersync plugin
// to create a dev server instance
// at http://localhost:9000
gulp.task('serve', ['nodemon'], function(done) {

  browserSync({

    open: false,
    baseDir: ['.'],
    middleware: function (req, res, next) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      next();
    },

    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:5007',

    // informs browser-sync to use the following port for the proxied app
    // notice that the default port is 3000, which would clash with our expressjs
    port: 9000

  }, done);

});
