var http = require('http');
var express = require('express');
var app = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var dataGenerator = require('./generateData.js');
var data = dataGenerator();
var router = require('json-server').router(data);
var jwt = require('jsonwebtoken');
var _ = require('lodash');

var config = {
  port: process.env.PORT || 5007,
  secret: 'SORT 2015 Demo'
};

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.post('/api/login', login);
app.use('/api', router);
app.use(express.static('public'));
app.use('/dist', express.static('dist'));
app.use('/jspm_packages', express.static('jspm_packages'));
app.use('/styles', express.static('styles'));
app.use(require('stylus').middleware('styles'));

var server = http.createServer(app);

server.listen(config.port);
server.on('error', err => {
  console.log(err);
});
server.on('listening', () => {
  console.log('listening on port: ', config.port);
});


function createToken(user) {
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresInMinutes: 60*5 });
}

function getUserScheme(req) {

  var username;
  var type;
  var userSearch = {};

  // The POST contains a username and not an email
  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }
  // The POST contains an email and not an username
  else if(req.body.email) {
    username = req.body.email;
    type = 'email';
    userSearch = { email: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

function login(req, res) {

  var userScheme = getUserScheme(req);

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }

  var user = _.find(data.users, userScheme.userSearch);

  if (!user) {
    return res.status(401).send({message:"The username or password don't match", user: user});
  }

  if (user.password !== req.body.password) {
    return res.status(401).send("The username or password don't match");
  }

  res.status(201).send({
    id_token: createToken(user)
  });
}
