// Specific settings for our application's
// authentication context. These will override
// the default settings provided by aureliauth

var config = {

  // Our Node API is being served from localhost:3001
  baseUrl: '/api',
  // Logins happen at the POST /login endpoint.
  loginUrl: 'login',
  // The API serves its tokens with a key of id_token which differs from
  // aureliauth's standard.
  tokenName: 'id_token',
  // Once logged in, we want to redirect the user to the home view.
  loginRedirect: '#/home',

}

export default config;
