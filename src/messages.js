import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';

@inject(HttpClient)
export class Messages{
  heading = 'Messages';
  messages = [];

  constructor(http){
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('/api/');
    });

    this.http = http;
  }

  activate(){
    return this.http.fetch('messages')
      .then(response => response.json())
      .then(messages => this.messages = messages);
  }
}
