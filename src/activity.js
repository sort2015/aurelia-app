import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';
import 'moment';

@inject(HttpClient)
export class Activity{
  heading = 'Activities';
  activities = [];

  constructor(http){
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('/api/');
    });

    this.http = http;
  }

  activate(){
    return this.http.fetch('activity')
      .then(response => response.json())
      .then(activities => this.activities = activities);
  }
}

export class RelativeValueConverter {
  toView(value){
    console.log('timestamp to relative', value);
    return value && moment(value).fromNow();
  }
}
export class DateValueConverter {
  dateFormat = new Intl.DateTimeFormat('en', {
    weekday: "long", year: "numeric", month: "short",
    day: "numeric", hour: "2-digit", minute: "2-digit"
  });

  toView(value){
    console.log('timestamp to date', value);
    return value && this.dateFormat.format(new Date(value));
  }
}
