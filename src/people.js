import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import 'fetch';

@inject(HttpClient)
export class People{
  heading = 'People';
  people = [];

  constructor(http){
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('/api/');
    });

    this.http = http;
  }

  activate(){
    return this.http.fetch('people')
      .then(response => response.json())
      .then(people => this.people = people);
  }
}
