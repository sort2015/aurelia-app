import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';

// Using Aurelia's dependency injection, we inject the AuthService
// with the @inject decorator
@inject(AuthService)

export class Login {

  heading = 'Login';

  // These view models will be given values
  // from the signup form user input
  username = 'bob';
  password = 'test';

  // Any signup errors will be reported by
  // giving this view model a value in the
  // catch block within the signup method
  signupError = '';

  constructor(auth) {
    this.auth = auth;
  };

  login() {

    // Object to hold the view model values passed into the signup method
    var userInfo = { username: this.username, password: this.password }

    return this.auth.login(userInfo)
      .then((response) => {
        console.log("Logged in!");
      })
      .catch(error => {
        this.signupError = error.response;
      });

  };
}
