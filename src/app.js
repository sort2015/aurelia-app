import {AuthorizeStep} from 'aurelia-auth';

export class App {
  configureRouter(config, router){
    config.title = 'Aurelia';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.map([
      { route: 'login',         name: 'login',        moduleId: 'login',        nav: false, title:'Login' },
      { route: 'logout',        name: 'logout',       moduleId: 'logout',       nav: false, title:'Logout' },

      { route: ['','home'],     name: 'home',         moduleId: 'home',         nav: true,  title:'Home',         auth: true},
      { route: 'users',         name: 'users',        moduleId: 'users',        nav: true,  title:'Users',        auth: true },
      { route: 'people',        name: 'people',       moduleId: 'people',       nav: true,  title:'People',       auth: true },
      { route: 'messages',      name: 'messages',     moduleId: 'messages',     nav: true,  title:'Messages',     auth: true },
      { route: 'activity',      name: 'activity',     moduleId: 'activity',     nav: true,  title:'Activity',     auth: true },
    ]);

    this.router = router;
  }
}
